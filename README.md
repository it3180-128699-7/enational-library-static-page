# Enational Library Static Page

## Bắt đầu

Cut trang theo feature nào thì để code vào folder tương ứng.

## Tools và extensions nên cài

- IDE: `VS Code`
- Lib: `Bootstrap` v5
- Extensions (in đậm là vô cùng nên dùng)
  1. **Live server** (chạy server local)
  2. Auto Close Tag (tự đóng tag)
  3. Auto Rename Tag (tự đổi tên tag)
  4. Bracket Pair Colorizer 2 (hiển thị màu dấu ngoặc đỡ lẫn)
  5. Color Highlight (hiển thi màu sắc trên editor với các mã màu)
  6. **Prettier** (format code = lệnh `Ctrl Shift I` hoặc `Shift Alt F`)
  7. indent-rainbow (nhìn block code dễ hơn)
  8. Visual Studio IntelliCode (gợi ý code hay dùng)

## Hướng dẫn

### Tham khảo file mẫu đã import bootstrap trong file `home/index.html`

### Với file HTML

Ví dụ khi cắt trang `/books`, thì khi gõ trên browser `{url}/books`, nó sẽ tìm đến file `index.html`
trong folder `books`.

Còn khi vào trang `{url}/books/detail.html`, nó sẽ tìm đến file `detail.html` tương tự.

### Với file CSS

- Với file css, mỗi file html sẽ có các file css đi kèm tương ứng.

- Với những file có dạng `index.html`, thì file `css` đi kèm thường là `style.css`, còn với dạng khác,
  tên file `css` trùng với tên file `html`

- Với những component (những cái thành phần nhỏ hơn) mà có style phức tạp, có thể tách ra làm riêng thành các file css nhỏ, tránh viết 1 file css quá dài.

- Đặt tên class theo quy tắc [**BEM**](https://topdev.vn/blog/bem-la-gi/)
